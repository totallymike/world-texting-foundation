module.exports = {
  purge: [
    './pages/**/*.tsx',
    './components/**/*.tsx',
    './styles/**/*.css',
  ],
  theme: {
    extend: {},
  },
  variants: {},
  plugins: [],
}
