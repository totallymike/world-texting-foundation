import { NextApiRequest, NextApiResponse } from 'next';
import { getAll, Acronym } from 'lib/backend';

export default async (
  req: NextApiRequest,
  res: NextApiResponse
): Promise<void> => {
  const count = parseInt(req.query['count'] as string, 10);
  const acronyms = await getAll();
  const max = acronyms.length - 1;
  const results: Acronym[] = [];
  for (let i = count; i > 0; i -= 1) {
    const index = Math.floor(Math.random() * max);
    results.push(acronyms[index]);
  }

  res.status(200).json(results);
};
