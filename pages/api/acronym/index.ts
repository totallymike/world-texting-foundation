import { NextApiRequest, NextApiResponse } from 'next';
import { fold } from 'fp-ts/lib/Either';
import { find, getAll, insert } from 'lib/backend';
import { pipe } from 'fp-ts/lib/pipeable';

const postAcronym = (
  req: NextApiRequest,
  res: NextApiResponse
): Promise<void> => {
  const acronym = req.body['acronym'];
  const definition = req.body['definition'];
  return insert({ acronym, definition }).then((e) =>
    pipe(
      e,
      fold(
        (error) => res.status(400).json({ message: error.message }),
        (acronym) => res.status(200).json(acronym)
      )
    )
  );
};

const getAcronyms = async (
  req: NextApiRequest,
  res: NextApiResponse
): Promise<void> => {
  const fromPoint: number = parseInt((req.query['from'] as string) ?? '0', 10);
  const limit: number = parseInt((req.query['limit'] as string) ?? '10', 10);
  const search = req.query['search'] as string | undefined;

  let results = await (search ? find(search) : getAll());
  results = results.slice(fromPoint);
  const hasMore = results.length > limit;
  res.setHeader('HasMore', hasMore.toString());
  results = results.slice(0, limit);
  res.status(200).json(results);
};

export default async (
  req: NextApiRequest,
  res: NextApiResponse
): Promise<void> => {
  switch (req.method) {
    case 'POST':
      return postAcronym(req, res);
    case 'GET':
    default:
      return getAcronyms(req, res);
  }
};
