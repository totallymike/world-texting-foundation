import { NextApiRequest, NextApiResponse } from 'next';
import { fold } from 'fp-ts/lib/Option';
import { get, Acronym, update, deleteAcronym } from 'lib/backend';
import { pipe } from 'fp-ts/lib/pipeable';

const getAcronym = async (
  req: NextApiRequest,
  res: NextApiResponse<Acronym>
): Promise<void> => {
  const {
    query: { acronym },
  } = req;

  pipe(
    await get(acronym as string),
    fold(
      () => {
        res.status(404);
      },
      (result) => {
        res.status(200).json(result);
      }
    )
  );
};

const putAcronym = async (
  req: NextApiRequest,
  res: NextApiResponse<Acronym | string>
): Promise<void> => {
  const abbrev = req.query['acronym'] as string;
  if (req.headers['authorization'] !== 'Bearer a-real-token') {
    return res.status(401).send('Unauthorized');
  }
  pipe(
    await update(abbrev, req.body['definition']),
    fold(
      () => res.status(400).send('Bad. Something bad happened'),
      (acronym) => res.status(200).json(acronym)
    )
  );
};

const deleteAcronymHandler = async (
  req: NextApiRequest,
  res: NextApiResponse<Acronym | string>
): Promise<void> => {
  const abbrev = req.query['acronym'] as string;
  if (req.headers['authorization'] !== 'Bearer a-real-token') {
    return res.status(401).send('Unauthoized');
  }
  await deleteAcronym(abbrev);
  res.status(200).send('Deleted');
};

export default async (
  req: NextApiRequest,
  res: NextApiResponse<Acronym | string>
): Promise<void> => {
  switch (req.method) {
    case 'PUT':
      return putAcronym(req, res);
    case 'DELETE':
      return deleteAcronymHandler(req, res);
    case 'GET':
    default:
      return getAcronym(req, res);
  }
};
