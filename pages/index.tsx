import Head from 'next/head';
import useSwr from 'swr';
import { Acronym, getAll } from 'lib/backend';
import { GetStaticProps } from 'next';

const fetcher = (url: string) => fetch(url).then((res) => res.json());

export default function Home({ data: initialData }: { data: Acronym[] }) {
  const { data, error } = useSwr<Acronym[]>('/api/acronym?limit=30', fetcher, {
    initialData,
  });
  if (error) {
    console.error(error);
    return (
      <div className="container mx-auto">
        <Head>
          <title>World Texting Foundatiion</title>
          <link rel="icon" href="/favicon.ico" />
        </Head>
        <main>
          <p>Uh oh an error</p>
        </main>
      </div>
    );
  }

  if (!data) return <div>Loading...</div>;

  return (
    <div className="container mx-auto">
      <Head>
        <title>World Texting Foundation</title>
        <link rel="icon" href="/favicon.ico" />
      </Head>

      <main>
        <h1 className="text-3xl">World Texting Foundation</h1>
        <ul>
          {data.map((acronym) => {
            return (
              <li
                key={acronym.acronym}
                className={
                  'border-r border-b border-l border-gray-400 lg:border-t lg:border-gray-400 bg-white rounded-b lg:rounded-b-none lg:rounded-r p-4 flex leading-normal mb-4'
                }
              >
                <div className="w-20 mr-2">{acronym.acronym}</div>
                <div>{acronym.definition}</div>
              </li>
            );
          })}
        </ul>
      </main>
    </div>
  );
}

export const getStaticProps: GetStaticProps = async () => {
  const data = (await getAll()).slice(0, 30);
  return {
    props: {
      data,
    },
  };
};
