import { filter, findFirst } from 'fp-ts/lib/Array';
import * as E from 'fp-ts/lib/Either';
import { Option, isNone, option, fromNullable } from 'fp-ts/lib/Option';
import * as T from 'fp-ts/lib/Task';
import { pipe } from 'fp-ts/lib/pipeable';
import { Do } from 'fp-ts-contrib/lib/Do';
import * as fuzzysort from 'fuzzysort';
import originalAcronyms from './acronyms.json';

let acronyms: Acronym[] = (originalAcronyms as {
  [key: string]: string;
}[]).flatMap((acronym) =>
  Object.entries(acronym).map(([key, value]) => ({
    acronym: key,
    definition: value,
  }))
);

type DbResult<T> = E.Either<Error, T>;

export interface Acronym {
  acronym: string;
  definition: string;
}

const findByAbbreviation = (abbrev: string): Option<Acronym> => {
  return pipe(
    acronyms,
    findFirst((record) => record.acronym === abbrev)
  );
};

export const get = async (acronym: string): Promise<Option<Acronym>> => {
  return findByAbbreviation(acronym);
};

export const getAll = async (): Promise<Acronym[]> => acronyms;

export const find = (search: string): Promise<Acronym[]> =>
  fuzzysort
    .goAsync(search, acronyms, {
      keys: ['acronym', 'definition'],
      // Fairly high threshold
      threshold: -500,
    })
    .then((results) => results.map((_) => _.obj));

export const insert = async (acronym: Acronym): Promise<DbResult<Acronym>> => {
  if (isNone(findByAbbreviation(acronym.acronym))) {
    acronyms.push(acronym);
    return E.right(acronym);
  }
  return E.left(new Error('Acronym already present'));
};

export const update = async (
  abbreviation: string,
  definition: string
): Promise<Option<Acronym>> => {
  return Do(option)
    .bindL('acronym', () => findByAbbreviation(abbreviation))
    .bindL('result', () => {
      let result: Acronym | null = null;
      acronyms = acronyms.map((a) => {
        if (a.acronym === abbreviation) {
          const a2 = {
            acronym: abbreviation,
            definition,
          };
          result = a2;
          return a2;
        }
        return a;
      });
      return fromNullable(result);
    })
    .return(({ result }) => result);
};

export const deleteAcronym = async (abbreviation: string): Promise<void> => {
  acronyms = pipe(
    acronyms,
    filter((acronym) => acronym.acronym !== abbreviation)
  );
};
