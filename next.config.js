const { PHASE_DEVELOPMENT_SERVER } = require('next/constants');

module.exports = (phase, { defaultConfig }) => {
  let apiHost = 'http://localhost:3000';
  switch (phase) {
    case PHASE_DEVELOPMENT_SERVER:
      apiHost = 'http://localhost:3000';
    default:
      apiHost = process.env.VERCEL_URL;
  }
  return {
    ...defaultConfig,
    env: {
      ...defaultConfig.env,
      apiHost,
    },
  };
};
